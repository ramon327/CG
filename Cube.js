/**
 * Define and draw a cube.
 */
/*jslint node: true */

/**
 * Define the vertices of the cube.
 * @param gl the gl context
 * @return {WebGLBuffer} the buffer containing the vertices
 */
function defineCubeVertices(gl) {
    "use strict";
    // define the vertices of the cube
    // we organize them now by side, so that we can have constant colored faces
    var vertices = [
        // back
        -0.5, -0.5, -0.5,       // v0
        0.5, -0.5, -0.5,        // v1
        0.5, 0.5, -0.5,         // v2
        -0.5, 0.5, -0.5,        // v3
                                // front
        -0.5, -0.5, 0.5,        // v4
        0.5, -0.5, 0.5,         // v5
        0.5, 0.5, 0.5,          // v6
        -0.5, 0.5, 0.5,         // v7
                                // right
        0.5, -0.5, -0.5,        // v8 = v1
        0.5, 0.5, -0.5,         // v9 = v2
        0.5, 0.5, 0.5,          // v10 = v6
        0.5, -0.5, 0.5,         // v11 = v5
                                // left
        -0.5, -0.5, -0.5,       // v12 = v0
        -0.5, 0.5, -0.5,        // v13 = v3
        -0.5, 0.5, 0.5,         // v14 = v7
        -0.5, -0.5, 0.5,        // v15 = v4
                                // top
        -0.5, 0.5, -0.5,        // v16 = v3
        -0.5, 0.5, 0.5,         // v17 = v7
        0.5, 0.5, 0.5,          // v18 = v6
        0.5, 0.5, -0.5,         // v19 = v2
                                //bottom
        -0.5, -0.5, -0.5,       // v20 = v0
        -0.5, -0.5, 0.5,        // v21 = v4
        0.5, -0.5, 0.5,         // v22 = v5
        0.5, -0.5, 0            // v23 = v1
    ];
    var bufferVertices = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferVertices);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    return bufferVertices;
}
/**
 * Define the color of the cube.
 *
 * @param gl the gl context
 * @param backColor the color of the back face
 * @param frontColor the color of the front face
 * @param rightColor the color of the right face
 * @param leftColor the color of the left face
 * @param topColor the color of the top face
 * @param bottomColor the color of the bottom face
 * @return {WebGLBuffer} the buffer object created for the color
 */
function defineCubeColors(gl, backColor, frontColor, rightColor, leftColor, topColor, bottomColor) {
    "use strict";

    // make 4 entries, one for each vertex
    var backSide = backColor.concat(backColor, backColor, backColor);
    var frontSide = frontColor.concat(frontColor, frontColor, frontColor);
    var rightSide = rightColor.concat(rightColor, rightColor, rightColor);
    var leftSide = leftColor.concat(leftColor, leftColor, leftColor);
    var topSide = topColor.concat(topColor, topColor, topColor);
    var bottomSide = bottomColor.concat(bottomColor, bottomColor, bottomColor);

    var allSides = backSide.concat(frontSide, rightSide, leftSide, topSide, bottomSide);

    var bufferColor = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferColor);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(allSides), gl.STATIC_DRAW);
    return bufferColor;
}

/**
 * Define the cube normals
 * @param gl the gl context
 * @returns {WebGLBuffer}the buffer containing the normals
 */

function defineCubeNormals(gl) {
    "use strict";
    var backNormal = [0.0, 0.0, -1.0];
    var frontNormal = [0.0, 0.0, 1.0];
    var rightNormal = [1.0, 0.0, 0.0];
    var leftNormal = [-1.0, 0.0, 0.0];
    var topNormal = [0.0, 1.0, 0.0];
    var bottomNormal = [0.0, -1.0, 0.0];

    // make 4 entries, one for each vertex
    var backSideNormal = backNormal.concat(backNormal, backNormal, backNormal);
    var frontSideNormal = frontNormal.concat(frontNormal, frontNormal, frontNormal);
    var rightSideNormal = rightNormal.concat(rightNormal, rightNormal, rightNormal);
    var leftSideNormal = leftNormal.concat(leftNormal, leftNormal, leftNormal);
    var topSideNormal = topNormal.concat(topNormal, topNormal, topNormal);
    var bottomSideNormal = bottomNormal.concat(bottomNormal, bottomNormal, bottomNormal);

    var allSidesNormal = backSideNormal.concat(frontSideNormal, rightSideNormal, leftSideNormal, topSideNormal, bottomSideNormal);

    var bufferNormals = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferNormals);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(allSidesNormal), gl.STATIC_DRAW);
    return bufferNormals;
}

/**
 * Define texture coordinates for the cube.
 */

function defineCubeTextureCoord(gl) {
    "use strict";
    var textureCoords = [
        0.0, 0.0,              // back
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        // front
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        // right
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        // left
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        // top
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        // bottom
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0
    ];
    var bufferTextures = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferTextures);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoords), gl.STATIC_DRAW);
    return bufferTextures;
}

/**
 * Define the cube sides
 * @param gl the gl context
 * @returns the buffer containing indices for drawing the sides of the cube with triangles
 */
function defineCubeSides(gl) {
    "use strict";
    var vertexIndices = [
        0, 2, 1, // face 0 (back)
        2, 0, 3,
        4, 5, 6, // face 1 (front)
        4, 6, 7,
        8, 9, 10, // face 2 (right)
        10, 11, 8,
        12, 15, 14, // face 3 (left)
        14, 13, 12,
        16, 17, 18, // face 4 (top)
        18, 19, 16,
        20, 23, 22, // face 5 (bottom)
        22, 21, 20
    ];
    var bufferSides = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, bufferSides);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(vertexIndices), gl.STATIC_DRAW);
    return bufferSides;
}

/**
 * Draw the cube.
 *
 * @param gl the gl context
 * @param aVertexPositionId the index of the vertex position attribute in the shader program
 * @param aVertexColorId the index of the vertex color attribute in the shader program
 * @param aVertexNormalId the index of the vertex normal attribute in the shader program
 */
function drawCube(gl, cube, aVertexPositionId, aVertexColorId, aVertexTextureCoordId, aVertexNormalId) {
    "use strict";

    // bind the buffer, so that the cube vertices are used
    gl.bindBuffer(gl.ARRAY_BUFFER, cube.bufferVertices);
    gl.vertexAttribPointer(aVertexPositionId, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(aVertexPositionId);

    // bind the buffer for color if used
    if (aVertexColorId != undefined) {
        gl.bindBuffer(gl.ARRAY_BUFFER, cube.bufferColor);
        gl.vertexAttribPointer(aVertexColorId, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(aVertexColorId);
    }

    // bind the buffer for the texture coordinates, if used
    if (aVertexTextureCoordId != undefined) {
        gl.bindBuffer(gl.ARRAY_BUFFER, cube.bufferTextureCoord);
        gl.vertexAttribPointer(aVertexTextureCoordId, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(aVertexTextureCoordId);
    }

    // bind the buffer for normals, if used
    if (aVertexNormalId != undefined) {
        gl.bindBuffer(gl.ARRAY_BUFFER, cube.bufferNormals);
        gl.vertexAttribPointer(aVertexNormalId, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(aVertexNormalId);
    }

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cube.bufferSides);
    gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);
}

/**
 * Define the cube, the cube is modelled at the origin
 * @param gl the gl context
 * @param backColor the color of the back face
 * @param frontColor the color of the front face
 * @param rightColor the color of the right face
 * @param leftColor the color of the left face
 * @param topColor the color of the top face
 * @param bottomColor the color of the bottom face
 */
function defineCube(gl, backColor, frontColor, rightColor, leftColor, topColor, bottomColor) {
    var cube = {};
    cube.bufferVertices = defineCubeVertices(gl);
    cube.bufferColor = defineCubeColors(gl, backColor, frontColor, rightColor, leftColor, topColor, bottomColor);
    cube.bufferTextureCoord = defineCubeTextureCoord(gl);
    cube.bufferSides = defineCubeSides(gl);
    cube.bufferNormals = defineCubeNormals(gl);
    return cube;
}

