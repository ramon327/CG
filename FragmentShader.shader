precision mediump float;

varying vec4 vFragmentColor;
varying vec2 vTextureCoord;
varying vec3 vNormals;
varying vec3 vPositionCameraSpace;

uniform sampler2D uSampler;
uniform vec3 uLightPosition;
uniform vec3 uLightColor;

void main() {
//    vec3 color = texture2D(uSampler, vec2(vTextureCoord.s, vTextureCoord.t)).rgb; // @Todo: switch for textures / Colors
    vec3 color = vec3(vFragmentColor);
    vec3 normal = normalize(vNormals);
    vec3 lightDirection = uLightPosition - vPositionCameraSpace;
    lightDirection = normalize(lightDirection);
    float diffuseLight = max(dot(normal, lightDirection), 0.0);
    if (diffuseLight < 0.0) {
        diffuseLight = -diffuseLight;
    }

//    gl_FragColor = vec4(diffuseLight * vec3(vFragmentColor), 1.0);
    gl_FragColor = vec4(diffuseLight * color, 1)* vec4(uLightColor, 1.0);
}